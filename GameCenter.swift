//
//  GameCenter.swift
//  Ninja Goo
//
//  Created by Andrew on 3/31/15.
//  Copyright (c) 2015 Koruja Studios. All rights reserved.
//

import GameKit



class GameCenter : NSObject, GKGameCenterControllerDelegate {
    
    var canUseGameCenter: Bool = true
    //var achievementsDictionary: NSMutableDictionary!

    override init(){
        
        super.init()
        
    }
    
    
    //initiate gamecenter
    func authenticateLocalPlayer(viewCtrl : UIViewController){
        
        var localPlayer = GKLocalPlayer.localPlayer()
        
        localPlayer.authenticateHandler = {(viewController, error) -> Void in
            
            if (viewController != nil) {
                viewCtrl.presentViewController(viewController, animated: true, completion: nil)
            }
                
            else {
                println((GKLocalPlayer.localPlayer().authenticated))
            }
        }
        
    }
    
    //send high score to leaderboard
    func saveHighscore(score:Int, leaderboard : String ) {
        
        //check if user is signed in
        if GKLocalPlayer.localPlayer().authenticated {
            
            var scoreReporter = GKScore(leaderboardIdentifier: leaderboard) //leaderboard id here
            
            scoreReporter.value = Int64(score) //score variable here (same as above)
            
            var scoreArray: [GKScore] = [scoreReporter]
            
            GKScore.reportScores(scoreArray, withCompletionHandler: {(error : NSError!) -> Void in
                if error != nil {
                    println("error")
                }
            })
            
        }
        
    }
    
    
    //shows leaderboard screen
    func showLeader(viewCtrl : UIViewController) {
        var gc = GKGameCenterViewController()
        gc.gameCenterDelegate = self
        viewCtrl.presentViewController(gc, animated: true, completion: nil)
    }


    //hides leaderboard screen
    func gameCenterViewControllerDidFinish(gameCenterViewController: GKGameCenterViewController!)
    {
        gameCenterViewController.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    
    
   
    
    
    
    
    // Game Center
//    let gameCenterPlayer=GKLocalPlayer.localPlayer()
//    var canUseGameCenter:Bool = false {
//        didSet{if canUseGameCenter == true {// load prev. achievments form Game Center
//            gameCenterLoadAchievements()}
//        }}
    var gameCenterAchievements=[String:GKAchievement]()
    
    // MARK: Game Center
    // load prev achievement granted to the player
    func gameCenterLoadAchievements(){
        // load all prev. achievements for GameCenter for the user to progress can be added
        var allAchievements=[GKAchievement]()
        
        GKAchievement.loadAchievementsWithCompletionHandler({ (allAchievements, error:NSError!) -> Void in
            if error != nil{
                println("Game Center: could not load achievements, error: \(error)")
            } else {
                for anAchievement in allAchievements  {
                    if let oneAchievement = anAchievement as? GKAchievement {
                        self.gameCenterAchievements[oneAchievement.identifier]=oneAchievement}
                }
            }
        })
    }
    // add progress to an achievement
    func gameCenterAddProgressToAnAchievement(progress:Double,achievementID:String) {
        if canUseGameCenter == true { // only update progress if user opt-in to use Game Center
            // lookup if prev progress is logged for this achievement = achievement is already know (and loaded) form Game Center for this user
            var lookupAchievement:GKAchievement? = gameCenterAchievements[achievementID]
            
            if let achievement = lookupAchievement {
                // found the achievement with the given achievementID, check if it already 100% done
                if achievement.percentComplete != 100 {
                    // set new progress
                    achievement.percentComplete = progress
                    if progress == 100.0  {achievement.showsCompletionBanner=true}  // show banner only if achievement is fully granted (progress is 100%)
                    
                    // try to report the progress to the Game Center
                    GKAchievement.reportAchievements([achievement], withCompletionHandler:  {(var error:NSError!) -> Void in
                        if error != nil {
                            println("Couldn't save achievement (\(achievementID)) progress to \(progress) %")
                        }
                    })
                }
                else {// achievemnt already granted, nothing to do
                    println("DEBUG: Achievement (\(achievementID)) already granted")}
            } else { // never added  progress for this achievement, create achievement now, recall to add progress
                println("No achievement with ID (\(achievementID)) was found, no progress for this one was recoreded yet. Create achievement now.")
                gameCenterAchievements[achievementID] = GKAchievement(identifier: achievementID)
                // recursive recall this func now that the achievement exist
                gameCenterAddProgressToAnAchievement(progress, achievementID: achievementID)
            }
        }
    }
    
}