//
//  GameScene.swift
//  MenuNuts
//
//  Created by Diego Brito on 04/08/15.
//  Copyright (c) 2015 Diego Brito. All rights reserved.
//

import SpriteKit
import AVFoundation

enum ColliderType: UInt32 {
    case Pedra = 1
    case Noz = 2
    case Top = 4
}

class GameScene: SKScene, SKPhysicsContactDelegate, AVAudioPlayerDelegate {
    
    //MARK: - Variables
    var avGame: AVAudioPlayer!
    var gameViewController: UIViewController!
    var numNozes: Int!
    
    // Parallax
    var speedBranch = 4.8
    var speedTree = 20
    var paralax1MovePointsPerSec: CGFloat = 50.0
    var paralax2MovePointsPerSec: CGFloat = 25.0
    var paralax3MovePointsPerSec: CGFloat = 12.5
    
    // Paralax bounds
    
    var lastUpdateTime: NSTimeInterval = 0
    var currentTime: NSTimeInterval = 0
    var tempoAcumulado: NSTimeInterval = 0
    var dt: NSTimeInterval = 0
    
    // Elements

    var leaf : SKSpriteNode!
    var squirrelIdle : SKSpriteNode!
    var scoreHud : SKSpriteNode!
    var homeButton : SKSpriteNode!
    var touchHome : SKSpriteNode!
    var score0: SKSpriteNode!
    var score00: SKSpriteNode!
    var score000: SKSpriteNode!
    var score0000: SKSpriteNode!
    var score00000: SKSpriteNode!
    var score000000: SKSpriteNode!
    var score0000000: SKSpriteNode!
    var score00000000: SKSpriteNode!
    var score000000000: SKSpriteNode!
    var score0000000000: SKSpriteNode!// Até 1kkk FODA-SE
    
    // Atlas
    let elementAtlas = SKTextureAtlas(named: "elements")
    let otherAtlas = SKTextureAtlas(named: "other")
    
    // Controll
    var scoreCount : Int = 0
    //var lastScore : UInt32 = 0
    var lastScore: Int = (NSUserDefaults.standardUserDefaults().integerForKey("score") as? Int)!


    var gameOver = false
    
    override func didMoveToView(view: SKView) {
        //numNozes = 4
        
        var error: NSError?
        let fileURL:NSURL = NSBundle.mainBundle().URLForResource("inGame", withExtension: "mp3")!
        self.avGame = AVAudioPlayer(contentsOfURL: fileURL, fileTypeHint: AVFileTypeMPEGLayer3, error: &error)
        if avGame == nil {
            if let e = error {
                println(e.debugDescription)
            }
        }
        avGame.delegate = self
        avGame.prepareToPlay()
        avGame.volume = 1.0
        toogleAVMenu()
        
        startParallax()
        startCharacterIdleWithLeaf()
        startHud()
        createBranch(true)
        createPhysicsBodyTop()
        //createScore()
        self.physicsWorld.contactDelegate = self
        runAction(SKAction.repeatActionForever(
            SKAction.sequence([
                SKAction.runBlock(addLeafInTop),
                SKAction.waitForDuration(0.8)
                ])
            ))
        
        runAction(SKAction.repeatActionForever(
            SKAction.sequence([
                SKAction.runBlock(addLeafInRightSide),
                SKAction.waitForDuration(0.8)
                ])
            ))

    }
    
    // MARK: - Delegates
    
    // MARK: SKScene Delegate
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        /* Called when a touch begins */
        
        var gameCenterScore = GameCenter()
        var menu = MenuScene(size: self.size)
        menu.gameCenterController = gameViewController
        
        for touch in (touches as! Set<UITouch>) {
            let location = touch.locationInNode(self)
            if (gameOver){
                if(touchHome.containsPoint(location)){
                    self.avGame.stop()
                    if lastScore < scoreCount {
                    
                        lastScore = (scoreCount as? Int)!
                        NSUserDefaults.standardUserDefaults().setInteger(lastScore, forKey: "score")
                        NSUserDefaults.standardUserDefaults().synchronize()
                        gameCenterScore.saveHighscore(lastScore, leaderboard: "score")

                        
                    }
                    
                    println(self.lastScore)
                    
                    self.view?.presentScene(menu)
                }
            }else{
                if(homeButton.containsPoint(location)){
                    self.avGame.stop()
                    self.view?.presentScene(menu)


                    
                }
            }
            gameCenterScore.gameCenterLoadAchievements()
            if (scoreCount == 10){
                
                gameCenterScore.gameCenterAddProgressToAnAchievement(100.0, achievementID: "score10")
            }
            if (scoreCount == 20){
                
                gameCenterScore.gameCenterAddProgressToAnAchievement(100.0, achievementID: "score20")
            }
            if (scoreCount == 30){
                
                gameCenterScore.gameCenterAddProgressToAnAchievement(100.0, achievementID: "score30")
            }
            if (scoreCount == 40){
                
                gameCenterScore.gameCenterAddProgressToAnAchievement(100.0, achievementID: "score40")
            }
            if (scoreCount == 50){
                
                gameCenterScore.gameCenterAddProgressToAnAchievement(100.0, achievementID: "score50")
            }
            if (scoreCount == 60){
                
                gameCenterScore.gameCenterAddProgressToAnAchievement(100.0, achievementID: "score60")
            }
            if (scoreCount == 70){
                
                gameCenterScore.gameCenterAddProgressToAnAchievement(100.0, achievementID: "score70")
            }
            if (scoreCount == 80){
                
                gameCenterScore.gameCenterAddProgressToAnAchievement(100.0, achievementID: "score80")
            }
            if (scoreCount == 90){
                
                gameCenterScore.gameCenterAddProgressToAnAchievement(100.0, achievementID: "score90")
            }
            if (scoreCount == 100){
                
                gameCenterScore.gameCenterAddProgressToAnAchievement(100.0, achievementID: "score100")
            }
        }
    }
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        if(!gameOver && numNozes > 0){
            animaEsquiloJogandoAPedra()
            let touch = touches.first as! UITouch
            let touchLocation = touch.locationInNode(self)
            
            let projectile = SKSpriteNode(imageNamed: "pedra_1")
            projectile.position = self.squirrelIdle.position
            projectile.setScale(0.5)
            
            let offset = touchLocation - projectile.position
            
            projectile.physicsBody = SKPhysicsBody(rectangleOfSize: CGSizeMake(projectile.size.width, projectile.size.height))
            projectile.physicsBody?.allowsRotation = false
            projectile.physicsBody?.dynamic = true
            projectile.physicsBody?.affectedByGravity = true
            projectile.physicsBody?.friction = 1.0
            projectile.physicsBody?.restitution = 0.2
            projectile.physicsBody?.angularDamping = 0.0
            projectile.physicsBody?.linearDamping = 1.5
            projectile.physicsBody?.categoryBitMask = ColliderType.Pedra.rawValue
            projectile.physicsBody?.contactTestBitMask = ColliderType.Noz.rawValue
            projectile.physicsBody?.collisionBitMask = ColliderType.Noz.rawValue
            projectile.physicsBody?.mass = 1.0
            projectile.physicsBody?.usesPreciseCollisionDetection = true
            
            addChild(projectile)
            
            let direction = offset.normalized()
            let shootAmount = direction * 1000
            let realDest = shootAmount + projectile.position
            let actionMove = SKAction.moveTo(realDest, duration: 1.8)
            let actionMoveDone = SKAction.removeFromParent()
            projectile.runAction(SKAction.sequence([actionMove, actionMoveDone]))
            numNozes = numNozes - 1
        }
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        
        if lastUpdateTime > 0 {
            dt = currentTime - lastUpdateTime
        } else {
            dt = 0
        }
        tempoAcumulado = tempoAcumulado + dt
        lastUpdateTime = currentTime
        
        moveParalax1()
        moveParalax2()
        moveParalax3()
    }
    
    // MARK: AVFoundation Delegate
    
    func audioPlayerDidFinishPlaying(player: AVAudioPlayer!, successfully flag: Bool) {
        println("finished playing \(flag)")
        player.play()
    }
    func audioPlayerDecodeErrorDidOccur(player: AVAudioPlayer!, error: NSError!) {
        println("\(error.localizedDescription)")
    }
    
    // MARK: SKPhysicsContact Delegate
    
    func didBeginContact(contact: SKPhysicsContact) {
        
        let contactMask = contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask
        
        if contactMask == ColliderType.Pedra.rawValue | ColliderType.Noz.rawValue {
            
            if(contact.bodyA.node?.name == "noz") {
                cair(contact.bodyA.node! as! SKSpriteNode)
            }
            else {
                cair(contact.bodyB.node! as! SKSpriteNode)
            }
            
            if speedBranch > 0.5 {
            
                speedBranch = speedBranch - 0.07
                paralax1MovePointsPerSec = paralax1MovePointsPerSec + CGFloat(self.speedTree)
                paralax2MovePointsPerSec = paralax2MovePointsPerSec + CGFloat(self.speedTree)
                paralax3MovePointsPerSec = paralax3MovePointsPerSec + CGFloat(self.speedTree)
            
            }
            
           
            scoreCount++;
            updateScore()
        }
        
        if contactMask == ColliderType.Top.rawValue | ColliderType.Noz.rawValue {
            descerBoneco()
        }
    }

    
    // MARK: - Create Elements
    
    func createPhysicsBodyTop() {
        let sprite = SKSpriteNode(color: SKColor.yellowColor(), size: CGSize(width: self.frame.width, height: 30))
        sprite.position = CGPoint(x: CGRectGetMinX(self.frame) + sprite.size.width / 2, y: CGRectGetMaxY(self.frame) + sprite.size.height)
        sprite.name = "top"
        addChild(sprite)
        sprite.physicsBody = SKPhysicsBody(rectangleOfSize: sprite.size)
        sprite.physicsBody?.categoryBitMask = ColliderType.Top.rawValue
        sprite.physicsBody?.contactTestBitMask = ColliderType.Noz.rawValue
        sprite.physicsBody?.collisionBitMask = 20

        sprite.physicsBody?.dynamic = true
        sprite.physicsBody?.affectedByGravity = false
        sprite.physicsBody?.usesPreciseCollisionDetection = true

    }
    
    func createBranch(left : Bool){
        
        numNozes = 4
        let startGameAnimation = SKTextureAtlas(named: "startGameAnimation")
        
        let nextIsLeft = left
        
        var textureName = "branch_right"
        var xPos = self.frame.maxX
        var xAnchor : CGFloat = 1
        
        if left {
            textureName = "branch_left"
            xPos = 0
            xAnchor = 0
        }
        
        var branch = SKSpriteNode(texture: startGameAnimation.textureNamed(textureName))
        branch.position = CGPoint(x: xPos, y:CGRectGetMinY(self.frame) - branch.size.height)
        branch.anchorPoint = CGPoint(x: xAnchor, y: 0.5)
        branch.setScale(self.frame.size.width/750)
        branch.zPosition = -0.3
        
        var nozPos = CGPoint(x: -branch.size.width, y:-45)
        if left {
            nozPos = CGPoint(x: branch.size.width + 80, y:-75)
        }
        
        var noz = SKSpriteNode(texture: SKTexture(imageNamed: "nos_1"))
        noz.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        noz.position = nozPos
        noz.name = "noz"
        noz.physicsBody = SKPhysicsBody(rectangleOfSize: CGSizeMake(noz.size.width, noz.size.height))
        noz.physicsBody?.dynamic = false
        noz.physicsBody?.categoryBitMask = ColliderType.Noz.rawValue
        noz.physicsBody?.contactTestBitMask = ColliderType.Pedra.rawValue
        noz.physicsBody?.collisionBitMask = ColliderType.Pedra.rawValue
        noz.physicsBody?.usesPreciseCollisionDetection = true

        branch.addChild(noz)
        
        
        let moveUp = SKAction.moveToY(CGRectGetMidY(self.frame)/6, duration: NSTimeInterval(self.speedBranch)/2 + 0.2)
        
        addChild(branch)
        
        branch.runAction(moveUp, completion : {
            
            self.createBranch(!nextIsLeft)
            
            branch.runAction(SKAction.moveToY(CGRectGetMaxY(self.frame) + noz.size.height, duration: NSTimeInterval(self.speedBranch)), completion : {
                branch.removeFromParent()
            })
        })
    }
    
    // MARK: - Control Class
    
    func descerBoneco() {
        let descer = SKAction.moveByX(0, y: -50, duration: 1)
        
        self.squirrelIdle.runAction(descer)
        
        if self.squirrelIdle.position.y < CGRectGetMinY(self.frame) {
            println("GAME OVER")
            self.gameOver = true
            homeButton.removeFromParent()

            touchHome = SKSpriteNode(imageNamed: "Home")
            var gameOver = SKSpriteNode(imageNamed: "gameOver")
            
            gameOver.anchorPoint = CGPointZero
            gameOver.position = CGPoint(x: CGRectGetMidX(self.frame) - gameOver.frame.width/2 , y: CGRectGetMidY(self.frame))
            addChild(gameOver)
            
            touchHome.anchorPoint = CGPointZero
            touchHome.setScale(0.4)
            touchHome.position = CGPoint(x: CGRectGetMidX(self.frame) - touchHome.frame.width/2 , y: CGRectGetMidY(self.frame) - touchHome.frame.height)
            addChild(touchHome)
            
            homeButton.removeFromParent()
            
        }
    }
    
    func animaEsquiloJogandoAPedra(){
        let atlas = SKTextureAtlas(named: "animacao_shoot")
        var animado = [SKTexture]()
        
        for var i=1; i<=atlas.textureNames.count; i++ {
            animado.append(atlas.textureNamed("shoot_\(i)"))
        }
        
        squirrelIdle.texture = animado[0]
        squirrelIdle.runAction(SKAction.animateWithTextures(animado, timePerFrame: 0.01))
    }
    
    func cair(node : SKSpriteNode){
        node.physicsBody?.dynamic = true
        node.physicsBody?.affectedByGravity = true
        node.physicsBody?.categoryBitMask = 0
    }
    
    // MARK: - StartTheBackground
    
    func startCharacterIdleWithLeaf() {
        let leafFrontAtlas = SKTextureAtlas(named: "leafFront")
        let leafBackAtlas = SKTextureAtlas(named: "leafBack")
        let characterIdleAtlas = SKTextureAtlas(named: "character_idle")
        
        // Character Idle
        
        var characterIdleAnimated = [SKTexture]()
        
        for var i=1; i<=characterIdleAtlas.textureNames.count; i++ {
            characterIdleAnimated.append(characterIdleAtlas.textureNamed("emotions_\(i)"))
        }
        
        squirrelIdle = SKSpriteNode(texture: characterIdleAnimated[0])
        squirrelIdle.setScale(self.frame.size.width/700)
        squirrelIdle.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMaxY(self.frame))
        squirrelIdle.zPosition = 1
        addChild(squirrelIdle)
        
        squirrelIdle.runAction(SKAction.repeatActionForever(
            SKAction.animateWithTextures(characterIdleAnimated,
                timePerFrame: 0.2,
                resize: false,
                restore: true)),
            withKey:"characterIdleAnimated")
        
        // Leaf front
        
        var leafFrontAnimated = [SKTexture]()
        
        for var i=1; i<=leafFrontAtlas.textureNames.count; i++ {
            leafFrontAnimated.append(leafFrontAtlas.textureNamed("front_\(i)"))
        }
        
        var leafFront = SKSpriteNode(texture: leafFrontAnimated[0])
        squirrelIdle.addChild(leafFront)
        
        leafFront.runAction(SKAction.repeatActionForever(
            SKAction.animateWithTextures(leafFrontAnimated,
                timePerFrame: 0.2,
                resize: false,
                restore: true)),
            withKey:"LeafFrontAnimated")
        
        // leaf back
        
        var leafBackAnimated = [SKTexture]()
        
        for var j=1; j<=leafBackAtlas.textureNames.count; j++ {
            leafBackAnimated.append(leafBackAtlas.textureNamed("back_\(j)"))
        }
        
        var leafBack = SKSpriteNode(texture: leafFrontAnimated[0])
        leafBack.zPosition = -0.5
        squirrelIdle.addChild(leafBack)
        
        leafBack.runAction(SKAction.repeatActionForever(
            SKAction.animateWithTextures(leafBackAnimated,
                timePerFrame: 0.2,
                resize: false,
                restore: true)),
            withKey:"LeafFrontAnimated")
        
        // Final
        
        squirrelIdle.runAction(SKAction.moveToY(squirrelIdle.size.height, duration: 2.0))
    }
    
    func startHud() {
        self.scene?.userInteractionEnabled = false
        
        scoreHud = SKSpriteNode(texture: elementAtlas.textureNamed("score"))
        scoreHud.setScale(self.frame.width/700)
        scoreHud.zPosition = 10
        scoreHud.position = CGPoint(x: (self.frame.maxX - scoreHud.size.width / 2) - 10 , y: self.frame.maxY + scoreHud.size.height / 2 )
        addChild(scoreHud)
        scoreHud.runAction(SKAction.moveToY((self.frame.maxY - scoreHud.size.height / 2)-10, duration: 1.0))
        
        score0 = SKSpriteNode(texture: elementAtlas.textureNamed("0"))
        score0.setScale(self.frame.width/700)
        score0.zPosition = 10
        score0.anchorPoint = CGPointZero
        score0.position = CGPoint(x: (self.frame.maxX - score0.size.width) - 5, y: self.frame.maxY + score0.size.height / 2 )
        addChild(score0)
        score0.runAction(SKAction.moveToY(((self.frame.maxY - score0.size.height / 2)-20) - self.scoreHud.size.height, duration: 1.0))
        
        homeButton = SKSpriteNode(imageNamed: "Home")
        homeButton.setScale(0.3)
        homeButton.zPosition = 10
        homeButton.position = CGPoint(x: self.frame.minX + homeButton.size.width / 2, y: self.frame.maxY + homeButton.size.height / 2)
        addChild(homeButton)
        homeButton.runAction(SKAction.moveToY(self.frame.maxY - homeButton.size.height / 2, duration: 1.0)) { () -> Void in
            self.scene?.userInteractionEnabled = true
        }
    }

    // MARK: - Parallax
    
    func startParallax() {
        for i in 0...1 {
            let background = paralax1()
            background.anchorPoint = CGPointZero
            background.position = CGPoint(x: 0, y: CGFloat(i)*background.size.height)
            background.name = "background1"
            addChild(background)
        }
        
        for i in 0...1 {
            let background2 = paralax2()
            background2.anchorPoint = CGPointZero
            background2.position = CGPoint(x: 0, y: CGFloat(i)*background2.size.height)
            background2.name = "background2"
            addChild(background2)
        }
        
        for i in 0...1 {
            let background3 = paralax3()
            background3.anchorPoint = CGPointZero
            background3.position = CGPoint(x: 0, y: CGFloat(i)*background3.size.height)
            background3.name = "background3"
            addChild(background3)
        }
    }
    
    func paralax1() -> SKSpriteNode{
        
        let backGroundNode = SKSpriteNode()
        backGroundNode.anchorPoint = CGPointZero
        backGroundNode.name = "background1"
        
        let backGround1 = SKSpriteNode(imageNamed: "arvore_1")
        backGround1.zPosition = -2
        backGround1.size = self.frame.size
        backGround1.anchorPoint = CGPointZero
        backGround1.position = CGPoint(x: 0 , y: 0)
        backGroundNode.addChild(backGround1)
        
        let backGround2 = SKSpriteNode(imageNamed: "arvore_1")
        backGround2.zPosition = -2
        
        backGround2.size = self.frame.size
        backGround2.anchorPoint = CGPointZero
        backGround2.position = CGPoint( x: 0 , y: backGround2.size.height)
        backGroundNode.addChild(backGround2)
        
        backGroundNode.size = CGSize(width: backGround1.size.width, height: backGround1.size.height*2)
        
        return backGroundNode
    }
    
    func paralax2() -> SKSpriteNode{
        
        let backGroundNode = SKSpriteNode()
        backGroundNode.anchorPoint = CGPointZero
        backGroundNode.name = "background2"
        
        let backGround1 = SKSpriteNode(imageNamed: "arvore_2")
        backGround1.zPosition = -3
        backGround1.size = self.frame.size
        backGround1.anchorPoint = CGPointZero
        backGround1.position = CGPoint(x: 0 , y: 0)
        backGroundNode.addChild(backGround1)
        
        let backGround2 = SKSpriteNode(imageNamed: "arvore_2")
        backGround2.zPosition = -3
        backGround2.size = self.frame.size
        backGround2.anchorPoint = CGPointZero
        backGround2.position = CGPoint( x: 0 , y: backGround2.size.height)
        backGroundNode.addChild(backGround2)
        
        backGroundNode.size = CGSize(width: backGround1.size.width, height: backGround1.size.height*2)
        
        return backGroundNode
    }
    
    func paralax3() -> SKSpriteNode{
        
        let backGroundNode = SKSpriteNode()
        backGroundNode.anchorPoint = CGPointZero
        backGroundNode.name = "background3"
        
        let backGround1 = SKSpriteNode(imageNamed: "arvore_3")
        backGround1.zPosition = -4
        backGround1.size = self.frame.size
        backGround1.anchorPoint = CGPointZero
        backGround1.position = CGPoint(x: 0 , y: 0)
        backGroundNode.addChild(backGround1)
        
        let backGround2 = SKSpriteNode(imageNamed: "arvore_3")
        backGround2.zPosition = -4
        backGround2.size = self.frame.size
        backGround2.anchorPoint = CGPointZero
        backGround2.position = CGPoint( x: 0 , y: backGround2.size.height)
        backGroundNode.addChild(backGround2)
        
        backGroundNode.size = CGSize(width: backGround1.size.width, height: backGround1.size.height*2)
        
        return backGroundNode
    }
    
    func moveParalax1(){
        
        enumerateChildNodesWithName("background1") { node, _ in
            
            let background = node as! SKSpriteNode
            let backgroundVelocity = CGPoint(x: 0, y: self.paralax1MovePointsPerSec)
            //let amountToMove = backgroundVelocity
            let amountToMove = backgroundVelocity * CGFloat(self.dt)
            background.position += amountToMove
            
            if background.position.y >= background.size.height { background.position = CGPoint(x: background.position.x ,y: background.position.y - background.size.height*2) }
        }
    }
    
    func moveParalax2(){
        
        enumerateChildNodesWithName("background2") { node, _ in
            
            let background = node as! SKSpriteNode
            let backgroundVelocity = CGPoint(x: 0, y: self.paralax2MovePointsPerSec)
            //let amountToMove = backgroundVelocity
            let amountToMove = backgroundVelocity * CGFloat(self.dt)
            background.position += amountToMove
            
            if background.position.y >= background.size.height { background.position = CGPoint(x: background.position.x ,y: background.position.y - background.size.height*2) }
        }
    }
    
    func moveParalax3(){
        
        enumerateChildNodesWithName("background3") { node, _ in
            
            let background = node as! SKSpriteNode
            let backgroundVelocity = CGPoint(x: 0, y: self.paralax3MovePointsPerSec)
            //let amountToMove = backgroundVelocity
            let amountToMove = backgroundVelocity * CGFloat(self.dt)
            background.position += amountToMove
            
            if background.position.y >= background.size.height { background.position = CGPoint(x: background.position.x ,y: background.position.y - background.size.height*2) }
        }
    }
    
    // MARK: - Random Leafs
    
    func addLeafInTop(){
        leaf = createLeafAnimated()
        leaf.setScale(random(min: 1.0, max: 2.0))
        
        let actualX = random(min: leaf.size.width/2, max: size.width - leaf.size.width/2)
        leaf.position = CGPoint(x: actualX, y: size.height + leaf.size.height/2)
        leaf.zPosition = -0.6
        addChild(leaf)
        
        let actualDuration = random(min: CGFloat(5.0), max: CGFloat(6.0))
        
        let actionMove = SKAction.moveTo(CGPoint(x: -leaf.size.width/2 , y: actualX), duration: NSTimeInterval(actualDuration))
        let actionMoveDone = SKAction.removeFromParent()
        leaf.runAction(SKAction.sequence([actionMove, actionMoveDone]))
    }
    
    func addLeafInRightSide(){
        leaf = createLeafAnimated()
        leaf.setScale(random(min: 1.0, max: 2.0))
        
        let actualY = random(min: self.frame.maxY, max: self.frame.maxY-self.frame.maxY/3)
        let destinationY = random(min: self.frame.minY + self.frame.maxY/3, max: self.frame.minY - self.frame.maxY/3)
        
        leaf.position = CGPoint(x: size.width + leaf.size.width/2, y: actualY)
        leaf.zPosition = -0.6
        addChild(leaf)
        
        let actualDuration = random(min: CGFloat(5.0), max: CGFloat(6.0))
        
        let actionMove = SKAction.moveTo(CGPoint(x: -leaf.size.width/2, y: destinationY), duration: NSTimeInterval(actualDuration))
        let actionMoveDone = SKAction.removeFromParent()
        leaf.runAction(SKAction.sequence([actionMove, actionMoveDone]))
    }
    
    func createLeafAnimated() -> SKSpriteNode{
        var leafAux : SKSpriteNode!
        var spriteAnimated = [SKTexture]()
        
        spriteAnimated.append(self.otherAtlas.textureNamed("other_6"))
        spriteAnimated.append(self.otherAtlas.textureNamed("other_7"))
        spriteAnimated.append(self.otherAtlas.textureNamed("other_8"))
        spriteAnimated.append(self.otherAtlas.textureNamed("other_7"))
        
        leafAux = SKSpriteNode(texture: spriteAnimated[0])
        
        leafAux.runAction(SKAction.repeatActionForever(
            SKAction.animateWithTextures(spriteAnimated,
                timePerFrame: 0.4,
                resize: false,
                restore: true)),
            withKey:"leafAnimated")
        
        return leafAux
    }
    
    func random() -> CGFloat {
        return CGFloat(Float(arc4random()) / 0xFFFFFFFF)
    }
    
    func random(#min: CGFloat, max: CGFloat) -> CGFloat {
        return random() * (max - min) + min
    }
    
    func toogleAVMenu() {
        var musicKey: Bool = NSUserDefaults.standardUserDefaults().objectForKey("music")!.boolValue
        if musicKey{
            avGame.play()
        }else if (avGame.playing){
            avGame.pause()
        }
    }
    
    
    func updateScore(){
        var placar = "\(scoreCount)"
        var arrayPlacar = Array(placar)
        
        if(count(placar) == 1) {
            score0.texture = elementAtlas.textureNamed(placar)
            score0.position = CGPoint(x: (self.frame.maxX - score0.size.width) - 5, y: score0.position.y)
        }else if(count(placar) == 2){
            score0.texture = elementAtlas.textureNamed(String(arrayPlacar[1]))
            
            if let s = score00{
                score00.texture = elementAtlas.textureNamed(String(arrayPlacar[0]))
            }else{
                score00 = SKSpriteNode(texture: elementAtlas.textureNamed(String(arrayPlacar[0])))
                score00.zPosition = 10
                score00.anchorPoint = CGPoint(x: 1, y: 0)
                score0.addChild(score00)
            }
        }else if (count(placar) == 3){
            score0.texture = elementAtlas.textureNamed(String(arrayPlacar[2]))
            score00.texture = elementAtlas.textureNamed(String(arrayPlacar[1]))
            
            if let s = score000{
                score000.texture = elementAtlas.textureNamed(String(arrayPlacar[0]))
            }else{
                score000 = SKSpriteNode(texture: elementAtlas.textureNamed(String(arrayPlacar[0])))
                score000.anchorPoint = CGPoint(x: 1, y: 0)
                score000.position = CGPoint(x: -score000.size.width, y: score000.position.y)
                score00.addChild(score000)
            }
        }else if(count(placar) == 4){
            score0.texture = elementAtlas.textureNamed(String(arrayPlacar[3]))
            score00.texture = elementAtlas.textureNamed(String(arrayPlacar[2]))
            score000.texture = elementAtlas.textureNamed(String(arrayPlacar[1]))
            
            if let s = score0000{
                score0000.texture = elementAtlas.textureNamed(String(arrayPlacar[0]))
            }else{
                score0000 = SKSpriteNode(texture: elementAtlas.textureNamed(String(arrayPlacar[0])))
                score0000.anchorPoint = CGPoint(x: 1, y: 0)
                score0000.position = CGPoint(x: -score0000.size.width, y: score0000.position.y)
                score000.addChild(score0000)
            }
        }else if (count(placar) == 5){
            score0.texture = elementAtlas.textureNamed(String(arrayPlacar[4]))
            score00.texture = elementAtlas.textureNamed(String(arrayPlacar[3]))
            score000.texture = elementAtlas.textureNamed(String(arrayPlacar[2]))
            score0000.texture = elementAtlas.textureNamed(String(arrayPlacar[1]))
            
            if let s = score00000{
                score00000.texture = elementAtlas.textureNamed(String(arrayPlacar[0]))
            }else{
                score00000 = SKSpriteNode(texture: elementAtlas.textureNamed(String(arrayPlacar[0])))
                score00000.anchorPoint = CGPoint(x: 1, y: 0)
                score00000.position = CGPoint(x: -score00000.size.width, y: score00000.position.y)
                score0000.addChild(score00000)
            }
        }else if(count(placar) == 6){
            score0.texture = elementAtlas.textureNamed(String(arrayPlacar[5]))
            score00.texture = elementAtlas.textureNamed(String(arrayPlacar[4]))
            score000.texture = elementAtlas.textureNamed(String(arrayPlacar[3]))
            score0000.texture = elementAtlas.textureNamed(String(arrayPlacar[2]))
            score00000.texture = elementAtlas.textureNamed(String(arrayPlacar[1]))
            
            if let s = score000000{
                score000000.texture = elementAtlas.textureNamed(String(arrayPlacar[0]))
            }else{
                score000000 = SKSpriteNode(texture: elementAtlas.textureNamed(String(arrayPlacar[0])))
                score000000.anchorPoint = CGPoint(x: 1, y: 0)
                score000000.position = CGPoint(x: -score000000.size.width, y: score000000.position.y)
                score00000.addChild(score000000)
            }
        }else if (count(placar) == 7){
            score0.texture = elementAtlas.textureNamed(String(arrayPlacar[6]))
            score00.texture = elementAtlas.textureNamed(String(arrayPlacar[5]))
            score000.texture = elementAtlas.textureNamed(String(arrayPlacar[4]))
            score0000.texture = elementAtlas.textureNamed(String(arrayPlacar[3]))
            score00000.texture = elementAtlas.textureNamed(String(arrayPlacar[2]))
            score000000.texture = elementAtlas.textureNamed(String(arrayPlacar[1]))
            
            if let s = score0000000{
                score0000000.texture = elementAtlas.textureNamed(String(arrayPlacar[0]))
            }else{
                score0000000 = SKSpriteNode(texture: elementAtlas.textureNamed(String(arrayPlacar[0])))
                score0000000.anchorPoint = CGPoint(x: 1, y: 0)
                score0000000.position = CGPoint(x: -score0000000.size.width, y: score0000000.position.y)
                score000000.addChild(score0000000)
            }
        }else if(count(placar) == 8){
            score0.texture = elementAtlas.textureNamed(String(arrayPlacar[7]))
            score00.texture = elementAtlas.textureNamed(String(arrayPlacar[6]))
            score000.texture = elementAtlas.textureNamed(String(arrayPlacar[5]))
            score0000.texture = elementAtlas.textureNamed(String(arrayPlacar[4]))
            score00000.texture = elementAtlas.textureNamed(String(arrayPlacar[3]))
            score000000.texture = elementAtlas.textureNamed(String(arrayPlacar[2]))
            score0000000.texture = elementAtlas.textureNamed(String(arrayPlacar[1]))
            
            if let s = score00000000{
                score00000000.texture = elementAtlas.textureNamed(String(arrayPlacar[0]))
            }else{
                score00000000 = SKSpriteNode(texture: elementAtlas.textureNamed(String(arrayPlacar[0])))
                score00000000.anchorPoint = CGPoint(x: 1, y: 0)
                score00000000.position = CGPoint(x: -score00000000.size.width, y: score00000000.position.y)
                score0000000.addChild(score00000000)
            }
        }else if(count(placar) == 9){
            score0.texture = elementAtlas.textureNamed(String(arrayPlacar[8]))
            score00.texture = elementAtlas.textureNamed(String(arrayPlacar[7]))
            score000.texture = elementAtlas.textureNamed(String(arrayPlacar[6]))
            score0000.texture = elementAtlas.textureNamed(String(arrayPlacar[5]))
            score00000.texture = elementAtlas.textureNamed(String(arrayPlacar[4]))
            score000000.texture = elementAtlas.textureNamed(String(arrayPlacar[3]))
            score0000000.texture = elementAtlas.textureNamed(String(arrayPlacar[2]))
            score00000000.texture = elementAtlas.textureNamed(String(arrayPlacar[1]))
            
            if let s = score000000000{
                score000000000.texture = elementAtlas.textureNamed(String(arrayPlacar[0]))
            }else{
                score000000000 = SKSpriteNode(texture: elementAtlas.textureNamed(String(arrayPlacar[0])))
                score000000000.anchorPoint = CGPoint(x: 1, y: 0)
                score000000000.position = CGPoint(x: -score000000000.size.width, y: score000000000.position.y)
                score00000000.addChild(score000000000)
            }
        }else if(count(placar) == 10){
            score0.texture = elementAtlas.textureNamed(String(arrayPlacar[9]))
            score00.texture = elementAtlas.textureNamed(String(arrayPlacar[8]))
            score000.texture = elementAtlas.textureNamed(String(arrayPlacar[7]))
            score0000.texture = elementAtlas.textureNamed(String(arrayPlacar[6]))
            score00000.texture = elementAtlas.textureNamed(String(arrayPlacar[5]))
            score000000.texture = elementAtlas.textureNamed(String(arrayPlacar[4]))
            score0000000.texture = elementAtlas.textureNamed(String(arrayPlacar[3]))
            score00000000.texture = elementAtlas.textureNamed(String(arrayPlacar[2]))
            score000000000.texture = elementAtlas.textureNamed(String(arrayPlacar[1]))
            
            if let s = score0000000000{
                score0000000000.texture = elementAtlas.textureNamed(String(arrayPlacar[0]))
            }else{
                score0000000000 = SKSpriteNode(texture: elementAtlas.textureNamed(String(arrayPlacar[0])))
                score0000000000.anchorPoint = CGPoint(x: 1, y: 0)
                score0000000000.position = CGPoint(x: -score0000000000.size.width, y: score0000000000.position.y)
                score000000000.addChild(score0000000000)
            }
        }
    }
    
//    func createScore(){
//        // 2
//        score00 = SKSpriteNode(texture: elementAtlas.textureNamed("0"))
//        score00.zPosition = 10
//        score00.anchorPoint = CGPoint(x: 1, y: 0)
//        score0.addChild(score00)
//        
//        // 3
//        score000 = SKSpriteNode(texture: elementAtlas.textureNamed("0"))
//        score000.anchorPoint = CGPoint(x: 1, y: 0)
//        score000.position = CGPoint(x: -score000.size.width, y: score000.position.y)
//        score00.addChild(score000)
//        
//        // 4
//        score0000 = SKSpriteNode(texture: elementAtlas.textureNamed("0"))
//        score0000.anchorPoint = CGPoint(x: 1, y: 0)
//        score0000.position = CGPoint(x: -score0000.size.width, y: score0000.position.y)
//        score000.addChild(score0000)
//        
//        // 5
//        score00000 = SKSpriteNode(texture: elementAtlas.textureNamed("0"))
//        score00000.anchorPoint = CGPoint(x: 1, y: 0)
//        score00000.position = CGPoint(x: -score00000.size.width, y: score00000.position.y)
//        score0000.addChild(score00000)
//        
//        // 6
//        score000000 = SKSpriteNode(texture: elementAtlas.textureNamed("0"))
//        score000000.anchorPoint = CGPoint(x: 1, y: 0)
//        score000000.position = CGPoint(x: -score000000.size.width, y: score000000.position.y)
//        score00000.addChild(score000000)
//        
//        // 7
//        score0000000 = SKSpriteNode(texture: elementAtlas.textureNamed("0"))
//        score0000000.anchorPoint = CGPoint(x: 1, y: 0)
//        score0000000.position = CGPoint(x: -score0000000.size.width, y: score0000000.position.y)
//        score000000.addChild(score0000000)
//        
//        // 8
//        score00000000 = SKSpriteNode(texture: elementAtlas.textureNamed("0"))
//        score00000000.anchorPoint = CGPoint(x: 1, y: 0)
//        score00000000.position = CGPoint(x: -score00000000.size.width, y: score00000000.position.y)
//        score0000000.addChild(score00000000)
//        
//        // 9
//        score000000000 = SKSpriteNode(texture: elementAtlas.textureNamed("0"))
//        score000000000.anchorPoint = CGPoint(x: 1, y: 0)
//        score000000000.position = CGPoint(x: -score000000000.size.width, y: score000000000.position.y)
//        score00000000.addChild(score000000000)
//        
//        // 10
//        score0000000000 = SKSpriteNode(texture: elementAtlas.textureNamed("0"))
//        score0000000000.anchorPoint = CGPoint(x: 1, y: 0)
//        score0000000000.position = CGPoint(x: -score0000000000.size.width, y: score0000000000.position.y)
//        score000000000.addChild(score0000000000)
//        
//    }

}
