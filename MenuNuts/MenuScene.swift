//
//  GameScene.swift
//  MenuNuts
//
//  Created by Diego Brito on 24/07/15.
//  Copyright (c) 2015 Diego Brito. All rights reserved.
//

import SpriteKit
import AVFoundation
import UIKit

struct PhysicsCategory {
    static let None      : UInt32 = 0
    static let All       : UInt32 = UInt32.max
    static let Leaf      : UInt32 = 0b1
    static let Character : UInt32 = 0b10
}

class MenuScene: SKScene, SKPhysicsContactDelegate, AVAudioPlayerDelegate {
    
    //MARK: - Variables
    
    //GameCenter
    
    var gameCenter : GameCenter!
    var gameCenterController: UIViewController!
    
    // Musics
    
    var avMenu: AVAudioPlayer!
    
    // Menu
    var logo : SKSpriteNode!
    var play : SKSpriteNode!
    var config : SKSpriteNode!
    var parallax3 : SKSpriteNode!
    var leaf : SKSpriteNode!
    var characterIdle : SKSpriteNode!
    var gc : SKSpriteNode!

    
    // Config
    var backButton: SKSpriteNode!
    var switchMusic: SKSpriteNode!
    var switchEffectSounds: SKSpriteNode!
    
    // Atlas
    let otherAtlas = SKTextureAtlas(named: "other")
    let parallaxAtlas = SKTextureAtlas(named: "parallax")
    let logoAnimatedAtlas = SKTextureAtlas(named: "logo")
    let playAnimatedAtlas = SKTextureAtlas(named: "playButton")
    
    // Controll Variables
    var isStart = true
    var menuView = true
    var configView = false
    
    // MARK: - Delegate's Method
    
    override func didMoveToView(view: SKView) {
        
        gameCenter = GameCenter()
        gameCenter.authenticateLocalPlayer(gameCenterController)
         
        
        
        physicsWorld.gravity = CGVectorMake(0, 0)
        physicsWorld.contactDelegate = self
        
        // Music
        var error: NSError?
        
        // Music "inGame.mp3"
        
//        let fileURL:NSURL = NSBundle.mainBundle().URLForResource("inGame", withExtension: "mp3")!
//        self.avMenu = AVAudioPlayer(contentsOfURL: fileURL, fileTypeHint: AVFileTypeMPEGLayer3, error: &error)
        
        // Músic "menu.mp3"
        
        let data = NSData(contentsOfFile: NSBundle.mainBundle().pathForResource("menu", ofType: "mp3")!)
        self.avMenu = AVAudioPlayer(data: data!, error: &error)
        
        if avMenu == nil {
            if let e = error {
                println(e.debugDescription)
            }
        }
        avMenu.delegate = self
        avMenu.prepareToPlay()
        avMenu.volume = 1.0
        toogleAVMenu()
        
        // Start Background
        
        startBackground()
        startSpriteLogo()
        startSpritePlayButton()
        startSpriteConfigButton()
        startSpriteGameCenter()
        
        runAction(SKAction.repeatActionForever(
            SKAction.sequence([
                SKAction.runBlock(addLeafInTop),
                SKAction.waitForDuration(0.8)
                ])
            ))
        
        runAction(SKAction.repeatActionForever(
            SKAction.sequence([
                SKAction.runBlock(addLeafInRightSide),
                SKAction.waitForDuration(0.8)
                ])
            ))
        
        isStart = false
    }
    
    // MARK: SKScene Delegate
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        /* Called when a touch begins */
        
        for touch in (touches as! Set<UITouch>) {
            let location = touch.locationInNode(self)
            let actionMove = SKAction.moveToY(self.frame.minY, duration: 0.5)
            let actionRemove = SKAction.removeFromParent()
            
            if(menuView) {
                if(play.containsPoint(location)){
                    self.userInteractionEnabled = false
                    menuView = false
                    configView = false
                    println("start the game")
                    config.runAction(SKAction.sequence([SKAction.moveToY(self.frame.minY, duration: 0.1), actionRemove]))
                    play.runAction(SKAction.sequence([actionMove, actionRemove]))
                   // gc.runAction(SKAction.sequence([actionMove, actionRemove]))
                    logo.runAction(SKAction.sequence([actionMove, actionRemove]), completion: { () -> Void in
                        // Start game
                        self.startTheGame()
                        self.userInteractionEnabled = true
                    })
                }else if(config.containsPoint(location)){
                    self.userInteractionEnabled = false
                    menuView = false
                    configView = true
                    println("config touches began")
                    // Remove menu
                    config.runAction(SKAction.sequence([SKAction.moveToY(self.frame.minY, duration: 0.1), actionRemove]))
                    play.runAction(SKAction.sequence([actionMove, actionRemove]))
                   // gc.runAction(SKAction.sequence([actionMove, actionRemove]))
                    logo.runAction(SKAction.sequence([actionMove, actionRemove]), completion: { () -> Void in
                        // Start config "view"
                        self.startSpriteBackButton()
                        self.startSpriteSwitchMusic()
                        self.startSpriteSwitchEffectSounds()
                        self.userInteractionEnabled = true
                    })
                }else if(gc.containsPoint(location)){
                    //self.userInteractionEnabled = false
                    //menuView = true
                    println("GC touches began")
                    
                    gameCenter.showLeader(gameCenterController)
                    
                }
            } else if (configView) {
                if(backButton.containsPoint(location)) {
                    self.userInteractionEnabled = false
                    menuView = true
                    configView = false
                    backButton.runAction(SKAction.sequence([actionMove, actionRemove]))
                    switchEffectSounds.runAction(SKAction.sequence([SKAction.moveToY(self.frame.minY, duration: 0.3), actionRemove]))
                    switchMusic.runAction(SKAction.sequence([actionMove, actionRemove]), completion: { () -> Void in
                        self.startSpriteLogo()
                        self.startSpritePlayButton()
                        self.startSpriteConfigButton()
                        self.userInteractionEnabled = true
                    })
                } else if (switchMusic.containsPoint(location)){
                    println("switch music changed")
                    var musicKey: Bool = NSUserDefaults.standardUserDefaults().objectForKey("music")!.boolValue
                    
                    if (musicKey) {
                        self.switchMusic.texture = self.otherAtlas.textureNamed("other_5")
                        NSUserDefaults.standardUserDefaults().setObject(false, forKey:"music")
                        NSUserDefaults.standardUserDefaults().synchronize()
                        toogleAVMenu()
                    } else {
                        self.switchMusic.texture = self.otherAtlas.textureNamed("other_4")
                        NSUserDefaults.standardUserDefaults().setObject(true, forKey:"music")
                        NSUserDefaults.standardUserDefaults().synchronize()
                        toogleAVMenu()
                    }
                } else if (switchEffectSounds.containsPoint(location)){
                    println("switch effect sounds changed")
                    var effectSoundKey: Bool = NSUserDefaults.standardUserDefaults().objectForKey("effectSound")!.boolValue
                    
                    if (effectSoundKey) {
                        self.switchEffectSounds.texture = self.otherAtlas.textureNamed("other_3")
                        NSUserDefaults.standardUserDefaults().setObject(false, forKey:"effectSound")
                        NSUserDefaults.standardUserDefaults().synchronize()
                    } else {
                        self.switchEffectSounds.texture = self.otherAtlas.textureNamed("other_2")
                        NSUserDefaults.standardUserDefaults().setObject(true, forKey:"effectSound")
                        NSUserDefaults.standardUserDefaults().synchronize()
                    }
                }
            }
        }
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
    
    // MARK: SKPhysicsContact Delegate
    
    func didBeginContact(contact: SKPhysicsContact) {
        var firstBody: SKPhysicsBody
        var secondBody: SKPhysicsBody
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        } else {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        if ((firstBody.categoryBitMask & PhysicsCategory.Leaf != 0) &&
            (secondBody.categoryBitMask & PhysicsCategory.Character != 0)) {
                characterDidCollideWithLeaf(firstBody.node as! SKSpriteNode, character: secondBody.node as! SKSpriteNode)
        }
        
    }
    
    // MARK: AVFoundation Delegate
    
    func audioPlayerDidFinishPlaying(player: AVAudioPlayer!, successfully flag: Bool) {
        println("finished playing \(flag)")
        player.play()
    }
    func audioPlayerDecodeErrorDidOccur(player: AVAudioPlayer!, error: NSError!) {
        println("\(error.localizedDescription)")
    }
    
    // MARK: - Starts
    
    // MARK: Start Background
    
    func startBackground() {
        
        // FOREST
        
        var parallax1 = SKSpriteNode(texture: parallaxAtlas.textureNamed("arvore_3"))
        parallax1.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame))
        parallax1.size = self.frame.size
        parallax1.zPosition = -1.0
        addChild(parallax1)
        
        var parallax2 = SKSpriteNode(texture: parallaxAtlas.textureNamed("arvore_2"))
        parallax2.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame))
        parallax2.size = self.frame.size
        parallax2.zPosition = -1.0
        addChild(parallax2)
        
        parallax3 = SKSpriteNode(texture: parallaxAtlas.textureNamed("arvore_1"))
        parallax3.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame))
        parallax3.size = self.frame.size
        parallax3.zPosition = -1.0
        addChild(parallax3)
        
        let startGameAnimation = SKTextureAtlas(named: "startGameAnimation")
        
        // LEFT BRANCH
        
        var leftBranch = SKSpriteNode(texture: startGameAnimation.textureNamed("branch_left"))
        leftBranch.position = CGPoint(x: 0.0, y:CGRectGetMidY(self.frame))
        leftBranch.anchorPoint = CGPoint(x: 0, y: leftBranch.anchorPoint.y)
        leftBranch.setScale(self.frame.size.width/750)
        leftBranch.zPosition = -0.3
        addChild(leftBranch)
        
        // RIGHT BRANCH WITH BRIGHT NUT
        
        var rightBranch1 : SKSpriteNode!
        let branchWithNutsAtlas = SKTextureAtlas(named: "branchWithNuts")
        var branchWithNutsAnimated = [SKTexture]()
        
        for var i=1; i<=branchWithNutsAtlas.textureNames.count; i++ {
            branchWithNutsAnimated.append(branchWithNutsAtlas.textureNamed("branchWithNuts_\(i)"))
        }
        
        rightBranch1 = SKSpriteNode(texture: branchWithNutsAnimated[0])
        rightBranch1.anchorPoint = CGPoint(x: 1, y: 1)
        rightBranch1.setScale(self.frame.size.width/700)
        rightBranch1.position = CGPoint(x:self.frame.maxX, y:self.frame.maxY - rightBranch1.size.height/4)
        rightBranch1.zPosition = -0.3
        addChild(rightBranch1)
        
        rightBranch1.runAction(SKAction.repeatActionForever(
            SKAction.animateWithTextures(branchWithNutsAnimated,
                timePerFrame: 0.1,
                resize: false,
                restore: true)),
            withKey:"rightBranchWithNutAnimated")
        
        //RIGHT BRANCH WITH LEAF
        
        var rightBranch = SKSpriteNode(texture: startGameAnimation.textureNamed("branch_right"))
        var leafFront = SKSpriteNode(texture: startGameAnimation.textureNamed("leafFront"))
        var leafBack = SKSpriteNode(texture: startGameAnimation.textureNamed("leafBack"))
        rightBranch.setScale(self.frame.size.width/680)
        rightBranch.zPosition = 0
        leafBack.zPosition = -0.42
        leafFront.zPosition = -0.3
        leafFront.anchorPoint = CGPoint(x: 1, y: 0)
        leafBack.anchorPoint = CGPoint(x: 1, y: 0)
        rightBranch.addChild(leafBack)
        rightBranch.addChild(leafFront)
        rightBranch.anchorPoint = CGPoint(x: 1, y: 0)
        rightBranch.position = CGPoint(x:self.frame.maxX, y:self.frame.minY + rightBranch.size.height/1.5)
        addChild(rightBranch)
        rightBranch.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: size.width, height: 1), center: CGPoint(x: CGRectGetMidX(rightBranch.centerRect), y: CGRectGetMidY(rightBranch.centerRect)))
        rightBranch.physicsBody?.categoryBitMask = PhysicsCategory.Leaf
        rightBranch.physicsBody?.contactTestBitMask = PhysicsCategory.Character
        rightBranch.physicsBody?.collisionBitMask = PhysicsCategory.None
        rightBranch.physicsBody?.affectedByGravity = false
        
        // CHARACTER IDLE
        
        startSquirrelIdle(position: CGPoint(x:leftBranch.size.width/1.7, y:(CGRectGetMidY(self.frame)) + leftBranch.size.height/1.5))
        
    }
    
    // MARK: Start Sprites Menu View
    
    func startSpriteLogo() {
        var logoAnimated = [SKTexture]()
        
        for var i=1; i<=logoAnimatedAtlas.textureNames.count; i++ {
            logoAnimated.append(logoAnimatedAtlas.textureNamed("logo_\(i)"))
        }
        
        logo = SKSpriteNode(texture: logoAnimated[0])
        logo.setScale((self.frame.size.width/900) * 1.1)
        
        if(isStart){
            logo.position = CGPoint(x:CGRectGetMidX(self.frame), y: self.frame.maxY-self.frame.maxY/3)
            addChild(logo)
            
        }else{
            logo.position = CGPoint(x:CGRectGetMidX(self.frame), y: self.frame.maxY + logo.size.height)
            addChild(logo)
            logo.runAction(SKAction.moveToY(self.frame.maxY-self.frame.maxY/3, duration: 0.5))
        }
        
        logo.runAction(SKAction.repeatActionForever(
            SKAction.animateWithTextures(logoAnimated,
                timePerFrame: 0.2,
                resize: false,
                restore: true)),
            withKey:"logoAnimated")
    }
    
    func startSpritePlayButton() {
        var playAnimated = [SKTexture]()
        for var i=1; i<=playAnimatedAtlas.textureNames.count; i++ {
            playAnimated.append(playAnimatedAtlas.textureNamed("sprite_\(i)"))
        }
        
        play = SKSpriteNode(texture: playAnimated[0])
        play.setScale(self.frame.size.width/700)
        
        if (isStart) {
            play.position = CGPoint(x:CGRectGetMidX(self.frame), y:self.frame.maxY-(self.frame.maxY/3)*1.2)
            addChild(play)
        } else {
            play.position = CGPoint(x: CGRectGetMidX(self.frame), y: self.frame.maxY  + play.size.height)
            addChild(play)
            play.runAction(SKAction.moveToY(self.frame.maxY-(self.frame.maxY/3)*1.2, duration: 0.5))
        }
        
        play.runAction(SKAction.repeatActionForever(
            SKAction.animateWithTextures(playAnimated,
                timePerFrame: 0.2,
                resize: false,
                restore: true)),
            withKey:"playAnimated")
    }
    
    func startSpriteConfigButton() {
        config = SKSpriteNode(texture: otherAtlas.textureNamed("other_1"))
        config.setScale(self.frame.size.width/700)
        if (isStart) {
            config.position = CGPoint(x:self.frame.maxX - config.size.width/1.5, y:self.frame.minY + config.size.height/1.5)
            addChild(config)
        }else{
            config.position = CGPoint(x:self.frame.maxX - config.size.width/1.5, y:self.frame.maxY + config.size.height)
            addChild(config)
            config.runAction(SKAction.moveToY(self.frame.minY+(config.size.height/1.5), duration: 0.5))
        }
    }
    
    func startSpriteGameCenter() {
        gc = SKSpriteNode(texture: otherAtlas.textureNamed("GameCenterTrophy"))
        gc.setScale(self.frame.size.width/800)
        if (isStart) {
            gc.position = CGPoint(x:self.frame.minX + gc.size.width/1.5, y:self.frame.minY + gc.size.height/1.5)
            addChild(gc)
        }else{
            gc.position = CGPoint(x:self.frame.minX + gc.size.width/1.5, y:self.frame.maxY + gc.size.height)
            addChild(gc)
            gc.runAction(SKAction.moveToY(self.frame.minY+(gc.size.height/1.5), duration: 0.5))
        }
    }
    
    // MARK: Start Sprites Config View
    
    func startSpriteBackButton() {
        backButton = SKSpriteNode(texture: otherAtlas.textureNamed("other_0"))
        backButton.setScale(self.frame.size.width/700)
        backButton.position = CGPoint(x:self.frame.minX + backButton.size.width/1.5, y:self.frame.maxY + config.size.height)
        addChild(backButton)
        backButton.runAction(SKAction.moveToY(self.frame.maxY-(backButton.size.height/1.5), duration: 0.5))
    }
    
    func startSpriteSwitchMusic() {
        var musicKey: Bool = NSUserDefaults.standardUserDefaults().objectForKey("music")!.boolValue
        
        if (musicKey) {
            self.switchMusic = SKSpriteNode(texture: self.otherAtlas.textureNamed("other_4"))
            toogleAVMenu()
        } else {
            self.switchMusic = SKSpriteNode(texture: self.otherAtlas.textureNamed("other_5"))
            toogleAVMenu()
        }
        
        switchMusic.setScale(self.frame.size.width/700)
        switchMusic.position =  CGPoint(x:CGRectGetMidX(self.frame), y: self.frame.maxY + switchMusic.size.height)
        addChild(switchMusic)
        switchMusic.runAction(SKAction.moveToY(self.frame.midY + switchMusic.size.height*2, duration: 0.5))
    }
    
    func startSpriteSwitchEffectSounds() {
        var effectSoundKey: Bool = NSUserDefaults.standardUserDefaults().objectForKey("effectSound")!.boolValue
        
        if (effectSoundKey) {
            self.switchEffectSounds = SKSpriteNode(texture: self.otherAtlas.textureNamed("other_2"))
        } else {
            self.switchEffectSounds = SKSpriteNode(texture: self.otherAtlas.textureNamed("other_3"))
        }
        
        switchEffectSounds.setScale(self.frame.size.width/700)
        switchEffectSounds.position = CGPoint(x:CGRectGetMidX(self.frame), y: self.frame.maxY + switchEffectSounds.size.height)
        addChild(switchEffectSounds)
        switchEffectSounds.runAction(SKAction.moveToY(self.frame.midY - switchEffectSounds.size.height, duration: 0.5))
    }
    
    // MARK: Start The Game
    
    func startTheGame() {
        let characterJumpAtlas = SKTextureAtlas(named: "character_jump")
        animateToJump(atlas: characterJumpAtlas)
    }
    
    func animateToJump(#atlas: SKTextureAtlas!) {
        var characterJump: SKSpriteNode!
        var characterJumpAnimated = [SKTexture]()
        
        for var i=1; i<=atlas.textureNames.count; i++ {
            characterJumpAnimated.append(atlas.textureNamed("jump_\(i)"))
        }
        
        characterJump = SKSpriteNode(texture: characterJumpAnimated[0])
        characterJump.setScale(self.frame.size.width/700)
        characterJump.position = characterIdle.position
        characterJump.zPosition = -0.4
        characterIdle.runAction(SKAction.removeFromParent())
        addChild(characterJump)
        
        characterJump.runAction(SKAction.animateWithTextures(characterJumpAnimated, timePerFrame: 0.1))
        characterJump.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: characterJump.size.width/4, height: characterJump.size.height/4), center: CGPoint(x: CGRectGetMidX(characterJump.centerRect), y: CGRectGetMidY(characterJump.centerRect)))
        
        characterJump.physicsBody?.allowsRotation = false
        scene?.physicsWorld.gravity = CGVector(dx: 0.0, dy: -6.0)
        
        if (size.width <= 320){
            characterJump.physicsBody?.velocity.dx = size.width/4
            characterJump.physicsBody?.velocity.dy = size.height
        }else{
            characterJump.physicsBody?.velocity.dx = size.width/4.5
            characterJump.physicsBody?.velocity.dy = size.height/1.1
        }
        
        characterJump.physicsBody?.categoryBitMask = PhysicsCategory.Character
        characterJump.physicsBody?.contactTestBitMask = PhysicsCategory.Leaf
        characterJump.physicsBody?.collisionBitMask = PhysicsCategory.None
        characterJump.physicsBody?.usesPreciseCollisionDetection = true
    }
    
    // MARK: - Other Starts
    
    func startSquirrelIdle(#position: CGPoint) -> SKSpriteNode{
        let characterIdleAtlas = SKTextureAtlas(named: "character_idle")
        var characterIdleAnimated = [SKTexture]()
        
        for var i=1; i<=characterIdleAtlas.textureNames.count; i++ {
            characterIdleAnimated.append(characterIdleAtlas.textureNamed("emotions_\(i)"))
        }
        
        characterIdle = SKSpriteNode(texture: characterIdleAnimated[0])
        characterIdle.setScale(self.frame.size.width/700)
        characterIdle.position = position
        characterIdle.zPosition = -0.4
        addChild(characterIdle)
        
        characterIdle.runAction(SKAction.repeatActionForever(
            SKAction.animateWithTextures(characterIdleAnimated,
                timePerFrame: 0.2,
                resize: false,
                restore: true)),
            withKey:"characterIdleAnimated")
        
        return characterIdle
    }
    
    func startSquirrelAnimationWithLeaf(#position: CGPoint) {
        var squirrelIdle = startSquirrelIdle(position: position)
        squirrelIdle.zPosition = -0.4
        
        let leafFrontAtlas = SKTextureAtlas(named: "leafFront")
        let leafBackAtlas = SKTextureAtlas(named: "leafBack")
        
        // FRONT
        
        var leafFrontAnimated = [SKTexture]()
        
        for var i=1; i<=leafFrontAtlas.textureNames.count; i++ {
            leafFrontAnimated.append(leafFrontAtlas.textureNamed("front_\(i)"))
        }
        
        var leafFront = SKSpriteNode(texture: leafFrontAnimated[0])
        squirrelIdle.addChild(leafFront)
        
        leafFront.runAction(SKAction.repeatActionForever(
            SKAction.animateWithTextures(leafFrontAnimated,
                timePerFrame: 0.2,
                resize: false,
                restore: true)),
            withKey:"LeafFrontAnimated")
        
        // BACK
        
        var leafBackAnimated = [SKTexture]()
        
        for var j=1; j<=leafBackAtlas.textureNames.count; j++ {
            leafBackAnimated.append(leafBackAtlas.textureNamed("back_\(j)"))
        }
        
        var leafBack = SKSpriteNode(texture: leafFrontAnimated[0])
        leafBack.zPosition = -0.42
        squirrelIdle.addChild(leafBack)
        
        leafBack.runAction(SKAction.repeatActionForever(
            SKAction.animateWithTextures(leafBackAnimated,
                timePerFrame: 0.2,
                resize: false,
                restore: true)),
            withKey:"LeafFrontAnimated")
        
        // Final
        
        squirrelIdle.runAction(SKAction.moveTo(CGPoint(x:CGRectGetMidX(self.frame), y:self.frame.minY), duration: 1.0), completion: { () -> Void in
            self.avMenu.stop()
            let gamescene = GameScene(size: self.size)
            gamescene.gameViewController = self.gameCenterController
            self.view?.presentScene(gamescene)
        })
        
    }
    
    // MARK: - Other Methods
    
    // MARK: Random Leafs
    
    func addLeafInTop(){
        leaf = createLeafAnimated()
        leaf.setScale(random(min: 1.0, max: 2.0))
        
        let actualX = random(min: leaf.size.width/2, max: size.width - leaf.size.width/2)
        leaf.position = CGPoint(x: actualX, y: size.height + leaf.size.height/2)
        leaf.zPosition = -0.6
        addChild(leaf)
        
        let actualDuration = random(min: CGFloat(5.0), max: CGFloat(6.0))
        
        let actionMove = SKAction.moveTo(CGPoint(x: -leaf.size.width/2 , y: actualX), duration: NSTimeInterval(actualDuration))
        let actionMoveDone = SKAction.removeFromParent()
        leaf.runAction(SKAction.sequence([actionMove, actionMoveDone]))
    }
    
    func addLeafInRightSide(){
        leaf = createLeafAnimated()
        leaf.setScale(random(min: 1.0, max: 2.0))
        
        let actualY = random(min: self.frame.maxY, max: self.frame.maxY-self.frame.maxY/3)
        let destinationY = random(min: self.frame.minY + self.frame.maxY/3, max: self.frame.minY - self.frame.maxY/3)
        
        leaf.position = CGPoint(x: size.width + leaf.size.width/2, y: actualY)
        leaf.zPosition = -0.6
        addChild(leaf)
        
        let actualDuration = random(min: CGFloat(5.0), max: CGFloat(6.0))
        
        let actionMove = SKAction.moveTo(CGPoint(x: -leaf.size.width/2, y: destinationY), duration: NSTimeInterval(actualDuration))
        let actionMoveDone = SKAction.removeFromParent()
        leaf.runAction(SKAction.sequence([actionMove, actionMoveDone]))
    }
    
    func createLeafAnimated() -> SKSpriteNode{
        var leafAux : SKSpriteNode!
        var spriteAnimated = [SKTexture]()
        
        spriteAnimated.append(self.otherAtlas.textureNamed("other_6"))
        spriteAnimated.append(self.otherAtlas.textureNamed("other_7"))
        spriteAnimated.append(self.otherAtlas.textureNamed("other_8"))
        spriteAnimated.append(self.otherAtlas.textureNamed("other_7"))
        
        leafAux = SKSpriteNode(texture: spriteAnimated[0])
        
        leafAux.runAction(SKAction.repeatActionForever(
            SKAction.animateWithTextures(spriteAnimated,
                timePerFrame: 0.4,
                resize: false,
                restore: true)),
            withKey:"leafAnimated")
        
        return leafAux
    }
    
    func random() -> CGFloat {
        return CGFloat(Float(arc4random()) / 0xFFFFFFFF)
    }
    
    func random(#min: CGFloat, max: CGFloat) -> CGFloat {
        return random() * (max - min) + min
    }
    
    // MARK: Collisions
    
    func characterDidCollideWithLeaf(leaf:SKSpriteNode, character:SKSpriteNode) {
        var characterPosition: CGPoint = character.position
        character.removeFromParent()
        for i in leaf.children{
            i.removeFromParent()
        }
        
        startSquirrelAnimationWithLeaf(position: characterPosition)
    }
    
    // MARK: Audio
    
    func toogleAVMenu() {
        var musicKey: Bool = NSUserDefaults.standardUserDefaults().objectForKey("music")!.boolValue
        if musicKey{
            avMenu.play()
        }else if (avMenu.playing){
            avMenu.pause()
        }
    }
}
